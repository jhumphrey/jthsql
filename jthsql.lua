-- this rewrite takes advantage of the -line option in sqlite3
jthsql = {

table_info = function(file, tbl)
        -- so we need a command string for sql.
        local cmdstr    = table.concat({".schema",tbl}," ")
        -- now we have our pipe string
        local pipestr   = table.concat({"echo \"",cmdstr,"\" | sqlite3 ",file})
        local pipe      = io.popen(pipestr, "r")
        local schema    = pipe:read("*all")
        pipe:close()

        -- check to see if we have anything in the pipe
        if schema == "" then
                return nil, "Not found"
        end
        -- for sqlite3 our querystring should be of the form "CREATE TABLE ([what we want]);"

        -- let's grab the good part ()
        local meat = schema:match("%((.-)%);").."," --TODO: this might hang upon () in strings.
        -- now, we want to go through each of the things, and put them into two tables

        local typetab = {}
        local columnlst = {}
        for col in meat:gmatch("(.-),") do
                local k         = col:match("%s*([%a%d]+)")
                local v         = col:match("%s*[%a%d]+%s*(%a+)")
                typetab[k]      = v
                table.insert(columnlst, k)
        end
        return typetab, columnlst
end,

-- here is a function to convert sql values to lua types
convert_val = function(val, dtype)
        -- so first we have to make sure we even have a val.
        if val and val ~= "" then
                -- now we have to see if we have a type
                if dtype then
                        if dtype == "TEXT" then
                                return val --TODO: should this be tostring?
                        elseif dtype == "NUMBER" or dtype == "INTEGER" then
                                return tonumber(val)
                        elseif dtype == "DATE" then
                                local y,m,d = val:match("(%d+)-(%d+)-(%d+)")
                                return os.time({year=y,month=m,day=d})
                        else
                                return nil, "UNKNOWN TYPE "..dtype
                        end
                else
                        return nil, "NO TYPE SPECIFIED"
                end
        end
end,

-- function to format a row of data.
format_row = function(line, typetab)
	local linetab = {}
	local prevkey
	local prevline = {}
	for field in string.gmatch(line,".-\n") do
		-- split into keyname = val
		local key,val = string.match(field,"^%s*(%w+)%s=%s(.+)")
		if not key then
			table.insert(prevline,"\n")
			table.insert(prevline,field)
		else
			if prevkey then
				linetab[prevkey], err = jthsql.convert_val(table.concat(prevline),typetab[prevkey])
if err then
	print('ERROR RECEIVED!!!!!!!!!')
	print(err)
end
			end
			prevkey=key
			prevline={val:sub(1,-2)} --chop off the new line
		end
			
	end

	--if prevkey then
	linetab[prevkey] = jthsql.convert_val(table.concat(prevline),typetab[prevkey])
	--end
	return linetab
end,

-- Utility function to get a whole table
gettable = function(file, tabname, typetab)
	typetab = typtab or jthsql.table_info(file, tabname)
	local cmdstr = table.concat({"echo \"SELECT * FROM ",tabname,";\" | sqlite3 -line ",file})
	local linetab = {}
	local restab = {}
	local prevkey
	local prevline = {}
	local pipe = io.popen(cmdstr, "r")
	local line=""
	while line do
		line = pipe:read()
		-- are we a blank vlaue?
		if not line then
			pipe:close()
			linetab[prevkey] = jthsql.convert_val(table.concat(prevline),typetab[prevkey])
			table.insert(restab,linetab)
			return restab
		end
		if line == "" then
			-- we are at a new entry
			-- TODO: what about descriptions with blank lines?
			if prevkey then
				linetab[prevkey]=jthsql.convert_val(table.concat(prevline),typetab[prevkey])
				table.insert(restab,linetab)
			end
			linetab={}
		end
		-- split into key = val
		
		local key,val = string.match(line,"^%s*(%w+)%s=%s(.+)")
		if not key then
			table.insert(prevline,line)
		else
			if prevkey then
				linetab[prevkey] = jthsql.convert_val(table.concat(prevline),typetab[prevkey])
			end
			prevkey=key
			prevline={val}
		end
	end
end,

do_cmd = function(cmd,file)
	local pipe = io.popen(table.concat({"echo \"",cmd,";\" | sqlite3 -line ",file}),"r")
	local result = pipe:read("*all")
	pipe:close()
	return result
end,

selectall = function(file,tab,condition,typetab)
	local resulttab = {}
	local resultstr = jthsql.do_cmd(table.concat({"SELECT * FROM",tab,"WHERE",condition}," "),file)
	for entry in (resultstr.."\n\n"):gmatch(".-\n\n") do
	--for entry in resultstr:gmatch("^[\n\n]") do
		table.insert(resulttab, jthsql.format_row(entry,typetab))
	end
	return resulttab
end,

select = function(file, tab, id, typetab)
	return jthsql.format_row(jthsql.do_cmd(table.concat({"SELECT",id,"FROM",tab}," "),file),typetab)
end,


createtable=function(file,tabname,typetab,columnlist)
	local cmdtab = {"CREATE table ", tabname, "( "}
	for i, cname in ipairs(columnlist) do
		if i > 1 then
			table.insert(cmdtab,", ")
		end
		table.insert(cmdtab, table.concat({"'",cname,"' ",typetab[cname]}))
	end
	table.insert(cmdtab,");")
	jthsql.do_cmd(table.concat(cmdtab),file)
end,

dumpval = function(val, valtype)
	if not val then 
		return "NULL"
	end
	if valtype == "TEXT" then
		return tostring(val)
	elseif valtype == "NUMBER" or valtype == "INTEGER" then
		return tostring(tonumber(val))
	elseif valtype == "DATE" then
		return os.date("%Y-%m-%d", val)
	else
		return nil, "UNKNOWN TYPE "..valtype
	end
end,

inserttab=function(file, tabname, fields, typetab, columnlist)
	cmdtab = {"INSERT INTO ",tabname, " values ("}
	for i, cname in ipairs(columnlist) do
		if i > 1 then
			--TODO KISS
			table.insert(cmdtab, ", ")
		end
		table.insert(cmdtab,table.concat({"'",jthsql.dumpval(fields[cname], typetab[cname]),"'"}))
	end
	table.insert(cmdtab, ");")
	jthsql.do_cmd(table.concat(cmdtab),file)
end,

}
