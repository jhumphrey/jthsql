require "jthsql"

-- we need to get info about this table first.
typetab, columnlist = jthsql.table_info("test.db","foo")
-- so now, we are going to add some entries:
jthsql.inserttab("test.db","foo",{bar="TEST TEXT", baz=os.time(), foobar=32}, typetab, columnlist)
jthsql.inserttab("test.db","foo",{bar="TEST ENTRY", baz=os.time({month=7, day=4, year=1776}), foobar=54}, typetab, columnlist)
jthsql.inserttab("test.db","foo",{bar="NEW ENTRY", baz=os.time({month=12,day=23,year=1}), foobar=2}, typetab, columnlist)
