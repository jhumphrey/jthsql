-- [BITS 16] ;lua isn't asm ;(
-- [ORG 0x7c00] ;and it doesn't let us do this either :=-(
-- NUF!!!!! LETS GET TO THIS!!
require "jthsql"
-- do a complete dump of the table.
-- first, we need to get the info for the table
typetab, columnlist = jthsql.table_info("test.db","foo")
-- now lets let's get the whole table, just cause.
footab = jthsql.gettable("test.db","foo",typetab, columnlist)
-- print the whole table.
for a,b in ipairs(footab) do
	print(a)
	for c, d in pairs(b) do
		print(c.." = "..d)
	end
end
-- now lets do a simple select * of all where foo like TEST
foolinetab = jthsql.selectall("test.db","foo","bar LIKE '%TEST%'",typetab, columnlist)

if not foolinetab then
	print(":(")
end
-- now let's dump everything from foolinetab.
print("---")
for a,b in ipairs(foolinetab) do
	print(a)
	for c, d in pairs(b) do
		print(c.." = "..d)
	end
end
