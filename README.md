# jthsql

A simple SQL library for lua


# FAQ
## What is this?
This is a simple library I wrote to handle SQL.
## Isn't there already luasql? 
I know, I know, I know. It is a bit too complex though. I wanted to make a *simple* libray.
## What can I do with it?
Not too much. You can implement features, or file issues if you aren't into implementing.
Right now here is what you can do:

* create tables
* insert values into those tables
* retreive tables
* select entries
* And execute any sql command, if you want to do the work to handle the output.

## Does this support all of SQL
~~Sure it does, it also supports clarvoiyance.~~
No it only supports what its user need and have implemented.
For example, only the following types are currently supported:

* TEXT
* NUMBER (or INTEGER)
* DATE

IF you have a type you especcially want supported. open an issue.

# Documentation
All functions have the jthsql prefix. (e.g. jthsql.*function*)
## table_info(file,tbl)
This function returns to tables, typetab, and columnlist
The former consists of pairs of the type *columnname*, *SQL type*
And the latter consists of ipairs of the columnnames, in order.
Both are required by other functions.
## *Internal Use* convert_val(val, dtype)
val &#9; The value you want to convert.
<br>
dtype &#9; The SQL type we want to convert
<br>
This function converts an SQL value to a lua value

Currently supported:
<table>
<tr><th>SQL TYPE</th><th>Lua type</th></tr>
<tr><td>TEXT</td><td>String</td></tr>
<tr><td>NUMBER</td><td>Number</td></tr>
<tr><td>INTEGER</td><td>Number</td></tr>
<tr><td>DATE</td><td>timestamp</td></tr>
</table>

## format_row(line,typetab,columnlist)
line &#9; The output line from sqlite3
<br>
**BUG ALERT:** Currently text with '|' is not supported
We probably should escape them, or use smart query
## gettable(file, tabname, typetab, columnlist)
This function basically converts a whole sql table to a lua table.
You should not be used if you are dealing with a large database.
## do_cmd(cmd,file)
this function tells sqlite3 to do some command
<br>
*cmd*&#9;A SQL command (The Semicolon at the end is auto added)
## selectall(file,tab,condition,typetab,columnlist)
this is kinda like your search function.
it executes a `SELECT * FROM tab WHERE condition;`
and returns the result as a table
## select(file, tab, id, typetab, columnlist)
this is like selectall, buut it returns one value
it executes `SELECT id FROM tab;`
## createtable(file,tabname,typetab,columnlist)
this creates a table called *tabname* with the columns in the order of *columntab* of types in typetab
## dumpval(val, valtype)
basically the inverse of convert_val takes a lua thing, and converts it tp specified SQL type
## inserttab(file, tabname, fields, typetab, columnlist)
This function inserts a row into the specified table. 
fields is in the form of a table consisting of pairs *columnname*,*value*
